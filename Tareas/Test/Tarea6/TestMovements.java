package Tarea6;
import static org.junit.Assert.*;
import org.junit.*;

public class TestMovements {
    Movements movements = new Movements();
    @Test
    public void testMovUp(){
        String[][] arrangement = new String[5][5];
        String[][] arrangement1 = new String[5][5];
        arrangement1[0][0] =null; arrangement1[0][1] =null; arrangement1[0][2] =null; arrangement1[0][3] =null;arrangement1[0][4] =null;
        arrangement1[1][0] =null; arrangement1[1][1] =null; arrangement1[1][2] =null; arrangement1[1][3] ="O ";arrangement1[1][4] =null;
        arrangement1[2][0] =null; arrangement1[2][1] =null; arrangement1[2][2] =null; arrangement1[2][3] ="- ";arrangement1[2][4] =null;
        arrangement1[3][0] =null; arrangement1[3][1] =null; arrangement1[3][2] =null; arrangement1[3][3] =null;arrangement1[3][4] =null;
        arrangement1[4][0] =null; arrangement1[4][1] =null; arrangement1[4][2] =null; arrangement1[4][3] =null;arrangement1[4][4] =null;
        assertArrayEquals(arrangement1,movements.movUp(arrangement,2,3));
    }
    @Test
    public void testMovUp2(){
        String[][] arrangement = new String[4][4];
        String[][] arrangement1 = new String[4][4];
        arrangement1[0][0] =null; arrangement1[0][1] =null; arrangement1[0][2] =null; arrangement1[0][3] =null;
        arrangement1[1][0] =null; arrangement1[1][1] =null; arrangement1[1][2] =null; arrangement1[1][3] ="O ";
        arrangement1[2][0] =null; arrangement1[2][1] =null; arrangement1[2][2] =null; arrangement1[2][3] ="- ";
        arrangement1[3][0] =null; arrangement1[3][1] =null; arrangement1[3][2] =null; arrangement1[3][3] =null;
        assertArrayEquals(arrangement1,movements.movUp(arrangement,2,3));
    }
    @Test
    public void testMovDown(){
        String[][] arrangement = new String[5][5];
        String[][] arrangement1 = new String[5][5];
        arrangement1[0][0] =null; arrangement1[0][1] =null; arrangement1[0][2] =null; arrangement1[0][3] =null;arrangement1[0][4] =null;
        arrangement1[1][0] =null; arrangement1[1][1] =null; arrangement1[1][2] =null; arrangement1[1][3] =null;arrangement1[1][4] =null;
        arrangement1[2][0] =null; arrangement1[2][1] =null; arrangement1[2][2] =null; arrangement1[2][3] ="- ";arrangement1[2][4] =null;
        arrangement1[3][0] =null; arrangement1[3][1] =null; arrangement1[3][2] =null; arrangement1[3][3] ="O ";arrangement1[3][4] =null;
        arrangement1[4][0] =null; arrangement1[4][1] =null; arrangement1[4][2] =null; arrangement1[4][3] =null;arrangement1[4][4] =null;
        assertArrayEquals(arrangement1,movements.movDown(arrangement,2,3));
    }
    @Test
    public void testMovDown2(){
        String[][] arrangement = new String[5][5];
        String[][] arrangement1 = new String[5][5];
        arrangement1[0][0] =null; arrangement1[0][1] =null; arrangement1[0][2] =null; arrangement1[0][3] =null;arrangement1[0][4] =null;
        arrangement1[1][0] =null; arrangement1[1][1] ="- "; arrangement1[1][2] =null; arrangement1[1][3] =null;arrangement1[1][4] =null;
        arrangement1[2][0] =null; arrangement1[2][1] ="O "; arrangement1[2][2] =null; arrangement1[2][3] =null;arrangement1[2][4] =null;
        arrangement1[3][0] =null; arrangement1[3][1] =null; arrangement1[3][2] =null; arrangement1[3][3] =null;arrangement1[3][4] =null;
        arrangement1[4][0] =null; arrangement1[4][1] =null; arrangement1[4][2] =null; arrangement1[4][3] =null;arrangement1[4][4] =null;
        assertArrayEquals(arrangement1,movements.movDown(arrangement,1,1));
    }
    @Test
    public void testMovRight(){
        String[][] arrangement = new String[5][5];
        String[][] arrangement1 = new String[5][5];
        arrangement1[0][0] =null; arrangement1[0][1] =null; arrangement1[0][2] =null; arrangement1[0][3] =null;arrangement1[0][4] =null;
        arrangement1[1][0] =null; arrangement1[1][1] =null; arrangement1[1][2] =null; arrangement1[1][3] =null;arrangement1[1][4] =null;
        arrangement1[2][0] =null; arrangement1[2][1] =null; arrangement1[2][2] =null; arrangement1[2][3] ="- ";arrangement1[2][4] ="O ";
        arrangement1[3][0] =null; arrangement1[3][1] =null; arrangement1[3][2] =null; arrangement1[3][3] =null;arrangement1[3][4] =null;
        arrangement1[4][0] =null; arrangement1[4][1] =null; arrangement1[4][2] =null; arrangement1[4][3] =null;arrangement1[4][4] =null;
        assertArrayEquals(arrangement1,movements.movRight(arrangement,2,3));
    }
    @Test
    public void testMovRight2(){
        String[][] arrangement = new String[3][3];
        String[][] arrangement1 = new String[3][3];
        arrangement1[0][0] ="- "; arrangement1[0][1] ="O "; arrangement1[0][2] =null;
        arrangement1[1][0] =null; arrangement1[1][1] =null; arrangement1[1][2] =null;
        arrangement1[2][0] =null; arrangement1[2][1] =null; arrangement1[2][2] =null;

        assertArrayEquals(arrangement1,movements.movRight(arrangement,0,0));
    }
    @Test
    public void testMovLeft(){
        String[][] arrangement = new String[5][5];
        String[][] arrangement1 = new String[5][5];
        arrangement1[0][0] =null; arrangement1[0][1] =null; arrangement1[0][2] =null; arrangement1[0][3] =null;arrangement1[0][4] =null;
        arrangement1[1][0] =null; arrangement1[1][1] =null; arrangement1[1][2] =null; arrangement1[1][3] =null;arrangement1[1][4] =null;
        arrangement1[2][0] =null; arrangement1[2][1] =null; arrangement1[2][2] ="O "; arrangement1[2][3] ="- ";arrangement1[2][4] =null;
        arrangement1[3][0] =null; arrangement1[3][1] =null; arrangement1[3][2] =null; arrangement1[3][3] =null;arrangement1[3][4] =null;
        arrangement1[4][0] =null; arrangement1[4][1] =null; arrangement1[4][2] =null; arrangement1[4][3] =null;arrangement1[4][4] =null;
        assertArrayEquals(arrangement1,movements.movLeft(arrangement,2,3));
    }
    @Test
    public void testMovLeft2(){
        String[][] arrangement = new String[5][5];
        String[][] arrangement1 = new String[5][5];
        arrangement1[0][0] =null; arrangement1[0][1] =null; arrangement1[0][2] =null; arrangement1[0][3] =null;arrangement1[0][4] =null;
        arrangement1[1][0] =null; arrangement1[1][1] =null; arrangement1[1][2] =null; arrangement1[1][3] =null;arrangement1[1][4] =null;
        arrangement1[2][0] =null; arrangement1[2][1] =null; arrangement1[2][2] =null; arrangement1[2][3] =null;arrangement1[2][4] =null;
        arrangement1[3][0] =null; arrangement1[3][1] =null; arrangement1[3][2] =null; arrangement1[3][3] =null;arrangement1[3][4] =null;
        arrangement1[4][0] =null; arrangement1[4][1] =null; arrangement1[4][2] =null; arrangement1[4][3] ="O ";arrangement1[4][4] ="- ";
        assertArrayEquals(arrangement1,movements.movLeft(arrangement,4,4));
    }
}
