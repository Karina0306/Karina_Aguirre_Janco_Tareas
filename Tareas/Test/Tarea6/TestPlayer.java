package Tarea6;
import static org.junit.Assert.*;
import org.junit.*;

public class TestPlayer {
    Player player = new Player();
    @Test
    public void mapPlayer(){
        String[][] arrangement = new String[5][5];
        String[][] arrangement1 = new String[5][5];
        arrangement1[0][0] =null; arrangement1[0][1] =null; arrangement1[0][2] =null; arrangement1[0][3] =null;arrangement1[0][4] =null;
        arrangement1[1][0] =null; arrangement1[1][1] =null; arrangement1[1][2] =null; arrangement1[1][3] =null;arrangement1[1][4] =null;
        arrangement1[2][0] =null; arrangement1[2][1] =null; arrangement1[2][2] ="O "; arrangement1[2][3] =null;arrangement1[2][4] =null;
        arrangement1[3][0] =null; arrangement1[3][1] =null; arrangement1[3][2] =null; arrangement1[3][3] =null;arrangement1[3][4] =null;
        arrangement1[4][0] =null; arrangement1[4][1] =null; arrangement1[4][2] =null; arrangement1[4][3] =null;arrangement1[4][4] =null;
        assertArrayEquals(arrangement1,player.positionPlayer(arrangement,2,2));
    }
    @Test
    public void mapPlayer2(){
        String[][] arrangement = new String[3][3];
        String[][] arrangement1 = new String[3][3];
        arrangement1[0][0] =null; arrangement1[0][1] =null; arrangement1[0][2] =null;
        arrangement1[1][0] =null; arrangement1[1][1] =null; arrangement1[1][2] ="O ";
        arrangement1[2][0] =null; arrangement1[2][1] =null; arrangement1[2][2] =null;
        assertArrayEquals(arrangement1,player.positionPlayer(arrangement,1,2));
    }
}
