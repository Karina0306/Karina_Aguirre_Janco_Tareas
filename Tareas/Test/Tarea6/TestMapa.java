package Tarea6;
import static org.junit.Assert.*;
import org.junit.*;

public class TestMapa {
    @Test
    public void mapTarea (){
        String[][] arrangement = new String[10][10];
        String[][] arrangement1 = new String[10][10];
        arrangement1[0][0] ="- "; arrangement1[0][1] ="- "; arrangement1[0][2] ="- "; arrangement1[0][3] ="X ";arrangement1[0][4] ="- ";arrangement1[0][5] ="- ";arrangement1[0][6] ="- ";arrangement1[0][7] ="- ";arrangement1[0][8] ="- ";arrangement1[0][9] ="- ";
        arrangement1[1][0] ="- "; arrangement1[1][1] ="- "; arrangement1[1][2] ="- "; arrangement1[1][3] ="- ";arrangement1[1][4] ="- ";arrangement1[1][5] ="- ";arrangement1[1][6] ="- ";arrangement1[1][7] ="- ";arrangement1[1][8] ="- ";arrangement1[1][9] ="- ";
        arrangement1[2][0] ="- "; arrangement1[2][1] ="- "; arrangement1[2][2] ="- "; arrangement1[2][3] ="- ";arrangement1[2][4] ="X ";arrangement1[2][5] ="- ";arrangement1[2][6] ="- ";arrangement1[2][7] ="- ";arrangement1[2][8] ="- ";arrangement1[2][9] ="- ";
        arrangement1[3][0] ="- "; arrangement1[3][1] ="- "; arrangement1[3][2] ="O "; arrangement1[3][3] ="- ";arrangement1[3][4] ="- ";arrangement1[3][5] ="- ";arrangement1[3][6] ="- ";arrangement1[3][7] ="- ";arrangement1[3][8] ="- ";arrangement1[3][9] ="- ";
        arrangement1[4][0] ="- "; arrangement1[4][1] ="- "; arrangement1[4][2] ="- "; arrangement1[4][3] ="- ";arrangement1[4][4] ="- ";arrangement1[4][5] ="- ";arrangement1[4][6] ="- ";arrangement1[4][7] ="X ";arrangement1[4][8] ="- ";arrangement1[4][9] ="- ";
        arrangement1[5][0] ="- "; arrangement1[5][1] ="- "; arrangement1[5][2] ="- "; arrangement1[5][3] ="- ";arrangement1[5][4] ="- ";arrangement1[5][5] ="- ";arrangement1[5][6] ="- ";arrangement1[5][7] ="- ";arrangement1[5][8] ="- ";arrangement1[5][9] ="- ";
        arrangement1[6][0] ="- "; arrangement1[6][1] ="- "; arrangement1[6][2] ="- "; arrangement1[6][3] ="- ";arrangement1[6][4] ="- ";arrangement1[6][5] ="- ";arrangement1[6][6] ="- ";arrangement1[6][7] ="- ";arrangement1[6][8] ="- ";arrangement1[6][9] ="- ";
        arrangement1[7][0] ="- "; arrangement1[7][1] ="- "; arrangement1[7][2] ="- "; arrangement1[7][3] ="- ";arrangement1[7][4] ="- ";arrangement1[7][5] ="- ";arrangement1[7][6] ="- ";arrangement1[7][7] ="- ";arrangement1[7][8] ="X ";arrangement1[7][9] ="- ";
        arrangement1[8][0] ="- "; arrangement1[8][1] ="- "; arrangement1[8][2] ="- "; arrangement1[8][3] ="- ";arrangement1[8][4] ="- ";arrangement1[8][5] ="- ";arrangement1[8][6] ="X ";arrangement1[8][7] ="- ";arrangement1[8][8] ="- ";arrangement1[8][9] ="- ";
        arrangement1[9][0] ="- "; arrangement1[9][1] ="- "; arrangement1[9][2] ="- "; arrangement1[9][3] ="- ";arrangement1[9][4] ="- ";arrangement1[9][5] ="- ";arrangement1[9][6] ="- ";arrangement1[9][7] ="- ";arrangement1[9][8] ="- ";arrangement1[9][9] ="- ";
        Mapa mapa = new Mapa();
        assertArrayEquals(arrangement1,mapa.mapGenerate(arrangement));
    }
}
