package Tarea12;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestRemove {
    @Test
    public void testRemoveString(){
        DoublyCircularLinkedList<String> list = new DoublyCircularLinkedList<>();
        assertTrue(list.isEmpty());
        list.add("Biologia");
        list.add("Programación");
        list.add("Historia");
        list.add("Matemática");
        list.add("Cálculo");
        list.add("Literatura");
        list.add("Sistemas");
        list.add("Ingles");
        list.add("Química");
        list.add("Fisica");
        list.add("Música");
        assertEquals(11,list.size());
        // remove from end
        assertEquals("Música", list.get(10));
        assertEquals("Fisica", list.get(9));
        assertTrue(list.remove("Música"));
        assertEquals(10,list.size());
        assertFalse(list.remove("Religión"));
        assertTrue(list.remove("Fisica"));
        assertEquals(9,list.size());
        assertEquals("Programación", list.get(10));
        assertEquals("Biologia", list.get(9));

        //remove from the beginning
        // list = Biologia,Programación,Historia,Matemática,Cálculo,Literatura,Sistemas,Ingles,Química
        assertEquals("Biologia", list.get(0));
        assertEquals("Programación", list.get(1));
        assertTrue(list.remove("Programación"));
        assertEquals(8,list.size());
        assertFalse(list.remove("Arte"));
        assertTrue(list.remove("Matemática"));
        assertEquals(7,list.size());
        assertEquals("Biologia", list.get(0));
        assertEquals("Historia", list.get(1));

        //remove from middle
        // list = Biologia,Historia,Cálculo,Literatura,Sistemas,Ingles,Química
        assertEquals("Literatura", list.get(3));
        assertEquals("Cálculo", list.get(2));
        assertTrue(list.remove("Literatura"));
        assertEquals(6,list.size());
        assertFalse(list.remove("Arte"));
        assertTrue(list.remove("Ingles"));
        assertEquals(5,list.size());
        assertEquals("Sistemas", list.get(3));
        assertEquals("Química", list.get(4));
    }
    @Test
    public void testRemoveInteger(){
        DoublyCircularLinkedList<Integer> list = new DoublyCircularLinkedList<>();
        assertEquals(true,list.isEmpty());
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);
        assertEquals(10,list.size());
        // remove from end
        // list : 1,2,3,4,5,6,7,8,9,10
        assertEquals(10, list.get(9));
        assertEquals(9, list.get(8));
        assertTrue(list.remove(10));
        assertEquals(9,list.size());
        assertFalse(list.remove(123));
        assertTrue(list.remove(9));
        assertEquals(8,list.size());
        assertEquals(3, list.get(10));
        assertEquals(2, list.get(9));

        //remove from the beginning
        // list : 1,2,3,4,5,6,7,8
        assertEquals(1, list.get(0));
        assertEquals(2, list.get(1));
        assertTrue(list.remove(1));
        assertEquals(8,list.size());
        assertFalse(list.remove(-1));
        assertTrue(list.remove(2));
        assertEquals(7,list.size());
        assertEquals(3, list.get(0));
        assertEquals(4, list.get(1));

        //remove from middle
        // list : 3,4,5,6,7,8
        assertEquals(6, list.get(3));
        assertEquals(5, list.get(2));
        assertTrue(list.remove(6));
        assertEquals(6,list.size());
        assertFalse(list.remove(10));
        assertTrue(list.remove(5));
        assertEquals(5,list.size());
        assertEquals(8, list.get(3));
        assertEquals(7, list.get(2));
    }
}
