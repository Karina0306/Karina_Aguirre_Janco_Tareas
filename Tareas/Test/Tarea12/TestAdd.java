package Tarea12;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestAdd {
    @Test
    public void testAddString(){
        DoublyCircularLinkedList<String> list = new DoublyCircularLinkedList<>();
        assertTrue(list.isEmpty());
        assertEquals(null,list.get(5));
        assertTrue(list.add("Monday"));
        assertFalse(list.add(null));
        assertTrue(list.add("Friday"));
        assertTrue(list.add("Sunday"));
        assertTrue(list.add("Saturday"));
        assertEquals(4,list.size());
        // add at the beginning
        list.addAtTheBeginning("week");
        assertEquals("week",list.get(0));
        list.addAtTheBeginning("Month");
        assertEquals("Month",list.get(0));
        assertEquals(6,list.size());
    }
    @Test
    public void testAddInteger(){
        DoublyCircularLinkedList<Integer> list = new DoublyCircularLinkedList<>();
        assertTrue(list.isEmpty());
        assertTrue(list.add(1));
        assertFalse(list.add(null));
        assertTrue(list.add(2));
        assertTrue(list.add(3));
        assertTrue(list.add(4));
        assertEquals(4,list.size());
        // add at the beginning
        list.addAtTheBeginning(0);
        assertEquals(0,list.get(0));
        list.addAtTheBeginning(-1);
        assertEquals(-1,list.get(0));
        assertEquals(6,list.size());
    }
}
