package Tarea12;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestXChange {
    @Test
    public void testXChangeString() {
        DoublyCircularLinkedList<String> list = new DoublyCircularLinkedList<>();
        assertTrue(list.isEmpty());
        list.add("June");
        list.add("April");
        list.add("May");
        list.add("March");
        list.add("January");
        list.add("February");
        //June,April,May,March,January,February
        assertEquals(6,list.size());
        assertEquals("June",list.get(0));
        list.xchange(0,4);
        assertEquals("January",list.get(0));
        //January,April,May,March,June,February
        assertEquals("April",list.get(1));
        list.xchange(1,5);
        assertEquals("February",list.get(1));
        //January,February,May,March,June,April
        assertEquals("May",list.get(2));
        list.xchange(2,3);
        assertEquals("March",list.get(2));
        //January,February,March,May,June,April
        assertEquals("May",list.get(3));
        list.xchange(3,5);
        assertEquals("April",list.get(3));
        //January,February,March,April,June,May
        assertEquals("June",list.get(4));
        list.xchange(4,5);
        assertEquals("May",list.get(4));
    }
    @Test
    public void testXChangeInteger() {
        DoublyCircularLinkedList<Integer> list = new DoublyCircularLinkedList<>();
        assertTrue(list.isEmpty());
        list.add(7);
        list.add(2);
        list.add(4);
        list.add(1);
        list.add(9);
        list.add(5);
        //7,2,4,1,9,5
        assertEquals(6,list.size());
        assertEquals(7,list.get(0));
        list.xchange(0,4);
        assertEquals(9,list.get(0));
        //9,2,4,1,7,5
        assertEquals(2,list.get(1));
        list.xchange(1,5);
        assertEquals(5,list.get(1));
        //9,5,4,1,7,2
        assertEquals(4,list.get(2));
        list.xchange(2,3);
        assertEquals(1,list.get(2));
        //9,5,1,4,7,2
        assertEquals(4,list.get(3));
        list.xchange(3,5);
        assertEquals(2,list.get(3));
        //9,5,1,2,7,4
        assertEquals(7,list.get(4));
        list.xchange(4,5);
        assertEquals(4,list.get(4));
    }
}
