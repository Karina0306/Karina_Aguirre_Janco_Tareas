package Tarea12;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestDoublyCircularLinkedList {
    @Test
    public void testDoublyCircularLinkedListInteger() {
        DoublyCircularLinkedList<Integer> list = new DoublyCircularLinkedList<Integer>();
        assertTrue(list.isEmpty());
        assertEquals(null,list.get(1));
        assertEquals(0,list.size());
        list.add(1);
        list.add(2);
        // 1,2
        assertEquals(2,list.size());
        assertEquals(2,list.get(1));
        list.addAtTheBeginning(3);
        assertEquals(3,list.get(0));
        list.addAtTheBeginning(4);
        //4,3,1,2
        assertEquals(4,list.size());
        assertEquals(3,list.get(1));
        list.add(7);
        //4,3,1,2,7
        assertEquals(5,list.size());
        assertEquals(2,list.get(13));
        list.remove(4);
        //3,1,2,7
        assertEquals(4,list.size());
        assertEquals(1,list.get(13));
        list.add(10);
        //3,1,2,7,10
        assertEquals(5,list.size());
        assertEquals(7,list.get(13));
        list.remove(45);
        list.remove(3);
        //1,2,7,10
        assertEquals(4,list.size());
        assertEquals(null,list.get(-6));
        list.add(6);
        //1,2,7,10,6
        assertEquals(5,list.size());
        assertEquals(6,list.get(4));
        list.remove(6);
        //1,2,7,10
        assertEquals(4,list.size());
        assertEquals(1,list.get(4));
        list.add(8);
        list.addAtTheBeginning(0);
        list.addAtTheBeginning(-1);
        list.add(12);
        //-1,0,1,2,7,10,8,12
        assertEquals(8,list.size());
        assertEquals(-1,list.get(0));
        list.remove(-1);
        //0,1,2,7,10,8,12
        assertEquals(7,list.size());
        assertEquals(null,list.get(-1));
        list.remove(38);
        list.remove(20);
        list.remove(1);
        list.remove(10);
        assertEquals(5,list.size());
        assertEquals(12,list.get(14));
        // 0,2,7,8,12
        list.remove(7);
        list.remove(12);
        assertEquals(3,list.size());
        // 0,2,8
        assertEquals(0, list.get(15));
    }
    @Test
    public void testDoublyCircularLinkedListString() {
        DoublyCircularLinkedList<String> list = new DoublyCircularLinkedList<String>();
        assertTrue(list.isEmpty());
        assertEquals(null,list.get(0));
        assertEquals(0,list.size());
        list.add("a");
        list.add("b");
        // a,b
        assertEquals(2,list.size());
        list.addAtTheBeginning("c");
        assertEquals("c",list.get(0));
        list.addAtTheBeginning("x");
        //x,c,a,b
        assertEquals(4,list.size());
        assertEquals("c",list.get(1));
        list.add("g");
        //x,c,a,b,g
        assertEquals(5,list.size());
        assertEquals("b",list.get(13));
        list.remove("c");
        //x,a,b,g
        assertEquals(4,list.size());
        assertEquals("a",list.get(13));
        list.add("k");
        //x,a,b,g,k
        assertEquals(5,list.size());
        assertEquals("g",list.get(13));
        list.remove("o");
        list.remove("x");
        //a,b,g,k
        assertEquals(4,list.size());
        assertEquals(null,list.get(-6));
        list.add("p");
        //a,b,g,k,p
        assertEquals(5,list.size());
        assertEquals("p",list.get(4));
        list.remove("p");
        //a,b,g,k
        assertEquals(4,list.size());
        assertEquals("a",list.get(4));
        list.add("f");
        list.addAtTheBeginning("z");
        list.addAtTheBeginning("y");
        list.add("d");
        //y,z,a,b,g,k,f,d
        assertEquals(8,list.size());
        assertEquals("y",list.get(0));
        list.remove("y");
        //z,a,b,g,k,f,d
        assertEquals(7,list.size());
        assertEquals(null,list.get(-5));
        list.remove("ñ");
        list.remove("m");
        list.remove("a");
        list.remove("f");
        assertEquals(5,list.size());
        assertEquals("d",list.get(14));
        // z,b,g,k,d
        list.remove("k");
        list.remove("g");
        assertEquals(3,list.size());
        // z,b,d
        assertEquals("z", list.get(15));
    }
}
