/*package Tarea8.appendNode;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class NodeTest2 {

    @Test
    public void twoEmpty() throws Exception {
        assertNull( Node2.append( null, null ) );
    }

    @Test
    public void oneEmpty() throws Exception {
        NodeHelper.assertEquals( Node2.append( null, new Node2( 1 ) ), new Node2( 1 ) );
        NodeHelper.assertEquals( Node2.append( new Node2( 1 ), null ), new Node2( 1 ) );
    }

    @Test
    public void oneOne() throws Exception {
        NodeHelper.assertEquals( Node2.append( new Node2( 1 ), new Node2( 2 ) ), NodeHelper.build( new int[] { 1, 2 } ) );
        NodeHelper.assertEquals( Node2.append( new Node2( 2 ), new Node2( 1 ) ), NodeHelper.build( new int[] { 2, 1 } ) );
    }

    @Test
    public void bigLists() throws Exception {
        NodeHelper.assertEquals(
                Node2.append( NodeHelper.build( new int[] { 1, 2 } ), NodeHelper.build( new int[] { 3, 4 } ) ),
                NodeHelper.build( new int[] { 1, 2, 3, 4 } )
        );
        NodeHelper.assertEquals(
                Node2.append( NodeHelper.build( new int[] { 1, 2, 3, 4, 5 } ), NodeHelper.build( new int[] { 6, 7, 8 } ) ),
                NodeHelper.build( new int[] { 1, 2, 3, 4, 5, 6, 7, 8 } )
        );
    }

}*/
