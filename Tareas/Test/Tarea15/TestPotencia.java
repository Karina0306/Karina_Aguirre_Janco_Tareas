package Tarea15;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestPotencia {
    @Test
    public void testPotenciaSimple() {
        CalculateSimple calculate = new CalculateSimple();
        int result = calculate.enhanceSimple("1^9");
        assertEquals(1,result);
        result = calculate.enhanceSimple("2^6");
        assertEquals(64,result);
        result = calculate.enhanceSimple("3^3");
        assertEquals(27,result);
        result = calculate.enhanceSimple("4^2");
        assertEquals(16,result);
        result = calculate.enhanceSimple("5^1");
        assertEquals(5,result);
        result = calculate.enhanceSimple("6^4");
        assertEquals(1296,result);
        result = calculate.enhanceSimple("7^5");
        assertEquals(16807,result);
        result = calculate.enhanceSimple("8^9");
        assertEquals(134217728,result);
        result = calculate.enhanceSimple("9^0");
        assertEquals(1,result);
        result = calculate.enhanceSimple("7^0");
        assertEquals(1,result);
    }
    @Test
    public void testPotenciaTwoDigits() {
        CalculateWithTwoDigits calculate = new CalculateWithTwoDigits();
        int result = calculate.enhanceTwoDigits("12^1");
        assertEquals(12,result);
        result = calculate.enhanceTwoDigits("10^2");
        assertEquals(100,result);
        result = calculate.enhanceTwoDigits("13^3");
        assertEquals(2197,result);
        result = calculate.enhanceTwoDigits("24^4");
        assertEquals(331776,result);
        result = calculate.enhanceTwoDigits("15^5");
        assertEquals(759375,result);
        result = calculate.enhanceTwoDigits("11^6");
        assertEquals(1771561,result);
        result = calculate.enhanceTwoDigits("16^7");
        assertEquals(268435456,result);
        result = calculate.enhanceTwoDigits("13^8");
        assertEquals(815730721,result);
        result = calculate.enhanceTwoDigits("10^9");
        assertEquals(1000000000,result);
        result = calculate.enhanceTwoDigits("57^0");
        assertEquals(1,result);
    }
}
