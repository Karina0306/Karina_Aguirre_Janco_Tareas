package Tarea15;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestSuma {
    @Test
    public void testSumaSimple() {
        CalculateSimple calculate = new CalculateSimple();
        int result = calculate.sumaSimple("1+2");
        assertEquals(3,result);
        result = calculate.sumaSimple("1+2+3");
        assertEquals(6,result);
        result = calculate.sumaSimple("1+2+3+4");
        assertEquals(10,result);
        result = calculate.sumaSimple("1+2+3+4+5");
        assertEquals(15,result);
        result = calculate.sumaSimple("1+5+2+4+5+7+2+0+3");
        assertEquals(29,result);
    }
    @Test
    public void testSumaDosDigitos() {
        CalculateWithTwoDigits calculate = new CalculateWithTwoDigits();
        int result = calculate.addTwoDigits("1+22");
        assertEquals(23,result);
        result = calculate.addTwoDigits("0+1214748364");
        assertEquals(1214748364,result);
        result = calculate.addTwoDigits("1+9999999");
        assertEquals(10000000,result);
        result = calculate.addTwoDigits("123+4");
        assertEquals(127,result);
        result = calculate.addTwoDigits("100000+1234");
        assertEquals(101234,result);
        result = calculate.addTwoDigits("12+45");
        assertEquals(57,result);
        result = calculate.addTwoDigits("1254+2455");
        assertEquals(3709,result);
        result = calculate.addTwoDigits("1010101+202020");
        assertEquals(1212121,result);
    }
}
