package Tarea15;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
public class MixedOperationsTest {
    @Test
    public void testSumaYMultiplicacion(){
        MixedOperations mixedOperations = new MixedOperations();
        int result = mixedOperations.OperationsSumaYMultiply("1+2*3*2");
        assertEquals(18,result);
        result = mixedOperations.OperationsSumaYMultiply("1+2*3+1");
        assertEquals(10,result);
        result = mixedOperations.OperationsSumaYMultiply("1+2*3*2+1+2");
        assertEquals(21,result);
    }
    @Test
    public void testSumaYPotencia(){
        MixedOperations mixedOperations = new MixedOperations();
        int result = mixedOperations.OperationsSumaYPotencia("1+2^2");
        assertEquals(9,result);
        result = mixedOperations.OperationsSumaYPotencia("1+2^2+1^2");
        assertEquals(100,result);
        result = mixedOperations.OperationsSumaYPotencia("1+0^2+1^2");
        assertEquals(4,result);
        result = mixedOperations.OperationsSumaYPotencia("3+2^2+1+9");
        assertEquals(35,result);
    }
    @Test
    public void testSumaYFactorial(){
        MixedOperations mixedOperations = new MixedOperations();
        int result = mixedOperations.OperationsSumaYFactorial("3+1!");
        assertEquals(24,result);
        result = mixedOperations.OperationsSumaYFactorial("3+1!+3");
        assertEquals(27,result);
        result = mixedOperations.OperationsSumaYFactorial("1+1!+3!");
        assertEquals(120,result);
        result = mixedOperations.OperationsSumaYFactorial("0+4!+1");
        assertEquals(25,result);
        result = mixedOperations.OperationsSumaYFactorial("0+2!+3!+4");
        assertEquals(124,result);
    }
}
