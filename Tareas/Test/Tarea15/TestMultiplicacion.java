package Tarea15;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class TestMultiplicacion {
    @Test
    public void testMutiplicacionSimple() {
        CalculateSimple calculate = new CalculateSimple();
        int result = calculate.multiplySimple("1*2");
        assertEquals(2,result);
        result = calculate.multiplySimple("6*5*3");
        assertEquals(90,result);
        result = calculate.multiplySimple("1*2*3*4*5*6*7");
        assertEquals(5040,result);
        result = calculate.multiplySimple("5*4");
        assertEquals(20,result);
    }
    @Test
    public void testMutiplicacionTwoDigits() {
        CalculateWithTwoDigits calculate = new CalculateWithTwoDigits();
        int result = calculate.multiplyTwoDigits("11*11");
        assertEquals(121,result);
        result = calculate.multiplyTwoDigits("65*3");
        assertEquals(195,result);
        result = calculate.multiplyTwoDigits("65*42");
        assertEquals(2730,result);
        result = calculate.multiplyTwoDigits("50*20");
        assertEquals(1000,result);
    }
}
