package Tarea15;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestFactorizacion {
    @Test
    public void testFactorizaciónSimple() {
        CalculateSimple calculate = new CalculateSimple();
        int result = calculate.factorialSimple("1!");
        assertEquals(1,result);
        result = calculate.factorialSimple("2!");
        assertEquals(2,result);
        result = calculate.factorialSimple("3!");
        assertEquals(6,result);
        result = calculate.factorialSimple("4!");
        assertEquals(24,result);
        result = calculate.factorialSimple("5!");
        assertEquals(120,result);
        result = calculate.factorialSimple("6!");
        assertEquals(720,result);
        result = calculate.factorialSimple("7!");
        assertEquals(5040,result);
        result = calculate.factorialSimple("8!");
        assertEquals(40320,result);
        result = calculate.factorialSimple("9!");
        assertEquals(362880,result);
    }
    @Test
    public void testFactorizaciónTwoDigits() {
        CalculateWithTwoDigits calculate = new CalculateWithTwoDigits();
        int result = calculate.factorialTwoDigits("11!");
        assertEquals(39916800,result);
        result = calculate.factorialTwoDigits("12!");
        assertEquals(479001600,result);
    }
}
