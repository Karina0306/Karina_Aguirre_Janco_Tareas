package Tarea15;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestsExceptions {
    @Test
    public void testRange() throws AccountNumberNotFoundException {
        Exceptions exception = new Exceptions();
        //exception.verifyRange(2147483648);
        assertTrue(exception.verifyRange(2147483647));
        assertTrue(exception.verifyRange(214483648));
        assertTrue(exception.verifyRange(0));
        //exception.verifyRange(-1);
    }
    @Test
    public void testTypeVariable() throws AccountNumberNotFoundException {
        Exceptions exception = new Exceptions();
        //exception.verifyTypeVariable(",");
        assertTrue(exception.verifyTypeVariable("67"));
        assertTrue(exception.verifyTypeVariable("8"));
        assertTrue(exception.verifyTypeVariable("0"));
        //exception.verifyTypeVariable(".");
    }
    @Test
    public void testBrackets() throws AccountNumberNotFoundException {
        Exceptions exception = new Exceptions();
        //exception.verifyBrackets(")()");
        exception.verifyBrackets("(1+2) + (1)(2)");
        assertTrue(exception.verifyBrackets("()"));
        assertTrue(exception.verifyBrackets("(4)+(7)*(4(2(7+8)))"));
        //exception.verifyBrackets("))");
    }
    @Test
    public void testOperations() throws AccountNumberNotFoundException {
        Exceptions exception = new Exceptions();
        //exception.verifyOperationPermitted("-");
        assertTrue(exception.verifyOperationPermitted("+"));
        //exception.verifyOperationPermitted("/");
        assertTrue(exception.verifyOperationPermitted("*"));
        assertTrue(exception.verifyOperationPermitted("!"));
        assertTrue(exception.verifyOperationPermitted("^"));
    }
    @Test
    public void testLetters() throws AccountNumberNotFoundException {
        Exceptions exception = new Exceptions();
        exception.verifyNotLetters("A + 4");
        //exception.verifyNotLetters("a + 4");
        assertTrue(exception.verifyNotLetters("3+5"));
        assertTrue(exception.verifyNotLetters("1+4+5*7!-4"));
        //exception.verifyNotLetters("1+4+5*7!-4 + c");
    }
}
