package Tarea9;
import org.junit.Test;
import java.util.List;
import java.util.ListIterator;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class TestJULinkedListIterator {
    @Test
    public void testIteratorString() {
        List<String> list = new JULinkedList();
        int counter = 0;
        while (list.iterator().hasNext()){
            assertEquals(list.get(counter), list.iterator().next());
        }
        assertEquals(0, counter);
    }
    @Test
    public void testIteratorInteger() {
        List<Integer> list = new JULinkedList();
        int counter = 0;
        while (list.iterator().hasNext()){
            assertEquals(list.get(counter), list.iterator().next());
        }
        assertEquals(0, counter);
    }
}
