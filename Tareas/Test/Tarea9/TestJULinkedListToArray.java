package Tarea9;
import org.junit.Test;
import java.util.List;
import java.util.ListIterator;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class TestJULinkedListToArray {
    @Test
    public void testToArrayString() {
        List<String> list = new JULinkedList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        String [] arr = new String[4];
        arr[0] = "a";
        arr[1] = "b";
        arr[2] = "c";
        arr[3] = "d";
        assertArrayEquals(arr,list.toArray());
    }
    @Test
    public void testToArrayString2() {
        List<String> list = new JULinkedList<>();
        list.add("hello");
        list.add("cat");
        list.add("bird");
        list.add("dog");
        String [] arr = new String[4];
        arr[0] = "hello";
        arr[1] = "cat";
        arr[2] = "bird";
        arr[3] = "dog";
        assertArrayEquals(arr,list.toArray());
    }
    @Test
    public void testToArrayInteger() {
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        Integer[] arr = new Integer[4];
        arr[0] = 1;
        arr[1] = 2;
        arr[2] = 3;
        arr[3] = 4;
        assertArrayEquals(arr,list.toArray());
    }
    @Test
    public void testToArrayInteger2() {
        List<Integer> list = new JULinkedList<>();
        list.add(12);
        list.add(23);
        list.add(34);
        list.add(45);
        Integer[] arr = new Integer[4];
        arr[0] = 12;
        arr[1] = 23;
        arr[2] = 34;
        arr[3] = 45;
        assertArrayEquals(arr,list.toArray());
    }
}
