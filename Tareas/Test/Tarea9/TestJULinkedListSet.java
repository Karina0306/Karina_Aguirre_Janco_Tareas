package Tarea9;
import org.junit.Test;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
public class TestJULinkedListSet {
    @Test
    public void testSetString(){
        List<String> list = new JULinkedList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        assertEquals("g",list.set(1,"g"));
        assertEquals("k",list.set(2,"k"));
        assertEquals("r",list.set(3,"r"));
        assertEquals(null,list.set(8,"l"));
        List<String> list1 = new JULinkedList<>();
        assertEquals(null,list1.set(4,"x"));
    }
    @Test
    public void testSetString2(){
        List<String> list = new JULinkedList<>();
        list.add("program");
        list.add("helo");
        list.add("jaba");
        list.add("stdent");
        assertEquals("hello",list.set(1,"hello"));
        assertEquals("java",list.set(2,"java"));
        assertEquals("student",list.set(3,"student"));
        assertEquals(null,list.set(8,"l"));
        List<String> list1 = new JULinkedList<>();
        assertEquals(null,list1.set(4,"x"));
    }
    @Test
    public void testGetInteger(){
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        assertEquals(Optional.ofNullable(5), Optional.ofNullable(list.set(1,5)));
        assertEquals(Optional.ofNullable(7), Optional.ofNullable(list.set(2,7)));
        assertEquals(Optional.ofNullable(123), Optional.ofNullable(list.set(3,123)));
        assertEquals(null,list.set(12,11));
        List<Integer> list1 = new JULinkedList<>();
        assertEquals(null,list1.set(0,7));
    }
    @Test
    public void testGetInteger2(){
        List<Integer> list = new JULinkedList<>();
        list.add(2022);
        list.add(2018);
        list.add(2031);
        list.add(2222);
        assertEquals(Optional.ofNullable(2019), Optional.ofNullable(list.set(1,2019)));
        assertEquals(Optional.ofNullable(2021), Optional.ofNullable(list.set(2,2021)));
        assertEquals(Optional.ofNullable(2022), Optional.ofNullable(list.set(3,2022)));
        assertEquals(null,list.set(12,2000));
        List<Integer> list1 = new JULinkedList<>();
        assertEquals(null,list1.set(0,1934));
    }
}
