package Tarea9;
import org.junit.Test;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class TestJULinkedListRemove {
    @Test
    public void testRemoveObjectString(){
        List<String> list = new JULinkedList<String>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        assertEquals(true,list.remove("b"));
        assertEquals(true,list.remove("d"));
        assertEquals(false,list.remove("g"));
        List<String> list2 = new JULinkedList<>();
        list2.add("a");
        list2.add("b");
        list2.add("c");
        list2.add("d");
        list2.remove("b");
        assertEquals(3, list2.size());
        List<String> list1 = new JULinkedList<>();
        assertEquals(false,list1.remove("a"));
    }
    @Test
    public void testRemoveObjectInteger(){
        Integer i = 2;
        Integer b = 3;
        Integer c = 8;
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        assertEquals(true,list.remove(i));
        assertEquals(true,list.remove(b));
        assertEquals(false,list.remove(c));
        List<Integer> list2 = new JULinkedList<>();
        list2.add(1);
        list2.add(2);
        list2.add(3);
        list2.add(4);
        list2.remove(i);
        assertEquals(3, list2.size());
        List<Integer> list1 = new JULinkedList<>();
        assertEquals(false,list1.remove(i));
    }
    @Test
    public void testRemoveIntString() {
        List<String> list = new JULinkedList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        assertEquals("b",list.remove(1));
        List<String> list1 = new JULinkedList<>();
        list1.add("a");
        list1.add("b");
        list1.add("c");
        list1.add("d");
        list1.remove(2);
        assertEquals(3, list1.size());
        List<String> list2 = new JULinkedList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("e");
        list.add("f");
        assertEquals("a",list.remove(0));
    }
    @Test
    public void testRemoveIntInteger(){
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        assertEquals(Optional.ofNullable(2), Optional.ofNullable(list.remove(1)));
        List<Integer> list2 = new JULinkedList<>();
        list2.add(1);
        list2.add(2);
        list2.add(3);
        list2.add(4);
        list2.remove(2);
        assertEquals(3, list2.size());
        List<Integer> list1 = new JULinkedList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);
        list1.add(4);
        assertEquals(Optional.ofNullable(4), Optional.ofNullable(list1.remove(3)));
    }
}
