package Tarea9;
import org.junit.Test;
import java.util.List;
import java.util.Optional;
import static org.junit.Assert.assertEquals;

public class TestJULinkedListGet {
    @Test
    public void testGetString(){
        List<String> list = new JULinkedList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        assertEquals("a",list.get(0));
        assertEquals("b",list.get(1));
        assertEquals("c",list.get(2));
        assertEquals("d",list.get(3));
        assertEquals(null,list.get(8));
        List<String> list1 = new JULinkedList<>();
        assertEquals(null,list1.get(4));
    }
    @Test
    public void testGetInteger(){
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        assertEquals(Optional.ofNullable(1), Optional.ofNullable(list.get(0)));
        assertEquals(Optional.ofNullable(2), Optional.ofNullable(list.get(1)));
        assertEquals(Optional.ofNullable(3), Optional.ofNullable(list.get(2)));
        assertEquals(Optional.ofNullable(4), Optional.ofNullable(list.get(3)));
        assertEquals(null,list.get(12));
        List<Integer> list1 = new JULinkedList<Integer>();
        assertEquals(null,list1.get(0));
    }
}
