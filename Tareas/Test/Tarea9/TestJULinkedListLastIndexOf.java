package Tarea9;
import org.junit.Test;

import java.util.List;
import static org.junit.Assert.assertEquals;

public class TestJULinkedListLastIndexOf {
    @Test
    public void testLastIndexOfString() {
        List<String> list = new JULinkedList<>();
        list.add("a");
        list.add("b");
        list.add("a");
        list.add("d");
        assertEquals(2,list.lastIndexOf("a"));
    }
    @Test
    public void testLastIndexOfString2() {
        List<String> list = new JULinkedList<>();
        list.add("hello");
        list.add("error");
        list.add("time");
        list.add("error");
        assertEquals(3,list.lastIndexOf("error"));
    }
    @Test
    public void testLastIndexOfString3() {
        List<String> list = new JULinkedList<>();
        list.add("hello");
        list.add("error");
        list.add("time");
        list.add("error");
        list.add("run");
        assertEquals(-1,list.lastIndexOf("correct"));
    }
    @Test
    public void testLastIndexOfString4() {
        List<String> list = new JULinkedList<>();
        list.add("system");
        list.add("program");
        list.add("student");
        list.add("ERROR");
        list.add("error");
        list.add("ERROR");
        list.add("java");
        list.add("java");
        list.add("c++");
        assertEquals(5,list.lastIndexOf("ERROR"));
    }
    @Test
    public void testLastIndexOfInteger() {
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        assertEquals(2,list.lastIndexOf(3));
    }
    @Test
    public void testLastIndexOfInteger2() {
        List<Integer> list = new JULinkedList<>();
        list.add(589);
        list.add(589);
        list.add(123);
        list.add(4);
        list.add(9);
        list.add(4);
        assertEquals(5,list.lastIndexOf(4));
    }
    @Test
    public void testLastIndexOfInteger3() {
        List<Integer> list = new JULinkedList<>();
        list.add(45);
        list.add(10);
        list.add(100);
        list.add(51);
        list.add(70);
        assertEquals(-1,list.lastIndexOf(1));
    }
}
