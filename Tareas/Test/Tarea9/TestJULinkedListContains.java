package Tarea9;
import org.junit.Test;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class TestJULinkedListContains {
    @Test
    public void testContainsString() {
        List<String> list = new JULinkedList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        assertEquals(true,list.contains("b"));
        assertEquals(false,list.contains("g"));
        assertEquals(true,list.contains("d"));
        assertEquals(false,list.contains("p"));
        assertEquals(false,list.contains("ab"));
    }
    @Test
    public void testContainsInteger() {
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        assertEquals(true,list.contains(4));
        assertEquals(false,list.contains(14));
        assertEquals(true,list.contains(8));
        assertEquals(false,list.contains(-1));
        assertEquals(false,list.contains(123));
    }
}
