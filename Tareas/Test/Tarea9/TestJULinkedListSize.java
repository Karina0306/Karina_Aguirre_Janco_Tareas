package Tarea9;
import org.junit.Test;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class TestJULinkedListSize {
    @Test
    public void testSizeString(){
        List<String> list = new JULinkedList<>();
        list.add("a");
        list.add("b");
        assertEquals(2, list.size());
        List<String> list1 = new JULinkedList<>();
        assertEquals(0, list1.size());
        List<String> list2 = new JULinkedList<>();
        list2.add("z");
        list2.add("y");
        list2.add("w");
        list2.add("w");
        list2.add("v");
        list2.add("u");
        list2.add("t");
        list2.add("s");
        assertEquals(8, list2.size());
    }
    @Test
    public void testSizeInteger(){
        List<Integer> list = new JULinkedList<>();
        list.add(3);
        list.add(6);
        list.add(3);
        list.add(6);
        assertEquals(4, list.size());
        List<Integer> list1 = new JULinkedList<>();
        assertEquals(0, list1.size());
        List<Integer> list2 = new JULinkedList<>();
        list2.add(1);
        list2.add(2);
        list2.add(3);
        list2.add(4);
        list2.add(5);
        list2.add(6);
        list2.add(7);
        assertEquals(7, list2.size());
    }
}
