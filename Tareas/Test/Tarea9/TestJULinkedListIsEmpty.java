package Tarea9;
import org.junit.Test;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class TestJULinkedListIsEmpty {
    @Test
    public void testIsEmptyString(){
        List<String> list = new JULinkedList<>();
        list.add("A");
        assertEquals(false,list.isEmpty());
        List<String> list1 = new JULinkedList<>();
        assertEquals(true, list1.isEmpty());
        List<String> list2 = new JULinkedList<>();
        list2.add("A");
        list2.add("b");
        list2.add("j");
        list2.add("f");
        list2.add("y");
        list2.add("s");
        assertEquals(false, list2.isEmpty());
    }
    @Test
    public void testIsEmptyInteger(){
        List<Integer> list = new JULinkedList<>();
        list.add(93583);
        assertEquals(false,list.isEmpty());
        List<Integer> list1 = new JULinkedList<>();
        assertEquals(true, list1.isEmpty());
        List<Integer> list2 = new JULinkedList<>();
        list2.add(56);
        list2.add(6);
        list2.add(32);
        list2.add(4);
        assertEquals(false, list2.isEmpty());
    }
}
