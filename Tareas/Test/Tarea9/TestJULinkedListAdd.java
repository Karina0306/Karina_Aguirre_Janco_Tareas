package Tarea9;
import org.junit.Test;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class TestJULinkedListAdd {
    @Test
    public void testAddString(){
        List<String> list = new JULinkedList<>();
        assertEquals(true, list.add("a"));
        assertEquals(true, list.add("b"));
        assertEquals(true, list.add("c"));
        assertEquals(true, list.add("d"));
        assertEquals(true, list.add("e"));
        List<String> list1 = new JULinkedList<>();
        assertEquals(false, list1.add(null));
    }
    @Test
    public void testAddInteger(){
        List<Integer> list = new JULinkedList<>();
        assertEquals(true, list.add(1));
        assertEquals(true, list.add(2));
        assertEquals(true, list.add(3));
        assertEquals(true, list.add(4));
        assertEquals(true, list.add(5));
        assertEquals(true, list.add(6));
        assertEquals(true, list.add(7));
        assertEquals(true, list.add(8));
        assertEquals(true, list.add(9));
        assertEquals(true, list.add(10));
        List<Integer> list1 = new JULinkedList<>();
        assertEquals(false, list1.add(null));
    }
}
