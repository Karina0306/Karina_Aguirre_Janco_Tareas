package Tarea9;
import org.junit.Test;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class TestJULinkedListClear {
    @Test
    public void testClearString() {
        List<String> list = new JULinkedList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        assertEquals(4,list.size());
        list.clear();
        assertEquals(0,list.size());
    }
    @Test
    public void testClearInteger() {
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        assertEquals(8,list.size());
        list.clear();
        assertEquals(0,list.size());
    }
}
