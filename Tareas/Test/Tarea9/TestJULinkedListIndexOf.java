package Tarea9;
import org.junit.Test;

import java.util.List;
import static org.junit.Assert.assertEquals;

public class TestJULinkedListIndexOf {
    @Test
    public void testIndexOfString() {
        List<String> list = new JULinkedList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        assertEquals(2,list.indexOf("c"));
    }
    @Test
    public void testIndexOfString2() {
        List<String> list = new JULinkedList<>();
        list.add("hello");
        list.add("error");
        list.add("time");
        list.add("error");
        assertEquals(1,list.indexOf("error"));
    }
    @Test
    public void testIndexOfString3() {
        List<String> list = new JULinkedList<>();
        list.add("hello");
        list.add("error");
        list.add("time");
        list.add("error");
        list.add("run");
        assertEquals(-1,list.indexOf("correct"));
    }
    @Test
    public void testIndexOfString4() {
        List<String> list = new JULinkedList<>();
        list.add("system");
        list.add("program");
        list.add("student");
        list.add("error");
        list.add("ERROR");
        list.add("java");
        list.add("java");
        list.add("c++");
        assertEquals(4,list.indexOf("ERROR"));
    }
    @Test
    public void testIndexOfInteger() {
        List<Integer> list = new JULinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        assertEquals(2,list.indexOf(3));
    }
    @Test
    public void testIndexOfInteger2() {
        List<Integer> list = new JULinkedList<>();
        list.add(589);
        list.add(123);
        list.add(4);
        list.add(9);
        assertEquals(0,list.indexOf(589));
    }
    @Test
    public void testIndexOfInteger3() {
        List<Integer> list = new JULinkedList<>();
        list.add(45);
        list.add(10);
        list.add(100);
        list.add(51);
        list.add(70);
        assertEquals(-1,list.indexOf(1));
    }
    @Test
    public void testIndexOfDate4() {
        List<Integer> list = new JULinkedList<>();
        list.add(2020-11-22);
        list.add(2019-10-23);
        list.add(2021-12-13);
        list.add(2022-6-21);
        list.add(2015-8-24);
        assertEquals(1, list.indexOf(2019-10-23));
    }
}
