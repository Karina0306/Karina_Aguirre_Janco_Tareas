package Tarea7;
import org.junit.*;

import java.util.Optional;

import static org.junit.Assert.*;

public class TestTarea7 {
    @Test
    public void testSize(){
        JUArrayList juArrayList = new JUArrayList();
        assertEquals(4,juArrayList.size());
    }
    @Test
    public void testIsEmpty(){
        JUArrayList juArrayList = new JUArrayList();
        assertEquals(true,juArrayList.isEmpty());
    }
    @Test
    public void testIterator(){
        JUArrayList juArrayList = new JUArrayList();
        assertEquals(3,juArrayList.iterator());
    }
    @Test
    public void testAdd(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.add(5);
        juArrayList.add(6);
        juArrayList.add(7);
        juArrayList.add(6);
        assertEquals(8,juArrayList.size());
    }
    @Test
    public void testGet(){
        JUArrayList juArrayList = new JUArrayList();
        assertEquals(Optional.of(5), Optional.of(juArrayList.get(4)));
        assertEquals(Optional.of(7), Optional.of(juArrayList.get(6)));
        assertEquals(Optional.of(9), Optional.of(juArrayList.get(8)));
    }
    @Test
    public void testRemoveO(){
        JUArrayList juArrayList = new JUArrayList();
        assertEquals(2,juArrayList.remove("1"));
    }
    @Test
    public void testClear(){
        JUArrayList juArrayList = new JUArrayList();
        juArrayList.clear();
    }
    @Test
    public void testRemoveIndex(){
        JUArrayList juArrayList = new JUArrayList();
        assertEquals(Optional.of(3),Optional.of(juArrayList.remove(2)));
    }
}
