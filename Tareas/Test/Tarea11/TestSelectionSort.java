package Tarea11;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestSelectionSort {
    @Test
    public void testSelectionSort(){
        LinkedBag<Integer> list = new LinkedBag<>();
        list.add(7);
        list.add(4);
        list.add(2);
        list.add(8);
        list.add(5);
        list.add(1);
        list.add(9);
        list.selectionSort();
        String a = String.valueOf(list);
        assertEquals("(7)root={data=1, sig->{data=2, sig->{data=4, sig->{data=5, sig->{data=7, sig->{data=8, sig->{data=9, sig->null}}}}}}}",a);
    }
    @Test
    public void testSelectionSort2(){
        LinkedBag<Integer> list = new LinkedBag<>();
        list.add(5);
        list.add(4);
        list.add(3);
        list.add(8);
        list.add(5);
        list.add(2);
        list.add(9);
        list.selectionSort();
        String a = String.valueOf(list);
        assertEquals("(7)root={data=2, sig->{data=3, sig->{data=4, sig->{data=5, sig->{data=5, sig->{data=8, sig->{data=9, sig->null}}}}}}}",a);
    }
    @Test
    public void testSelectionSort3(){
        LinkedBag<Float> list = new LinkedBag<>();
        list.add(5.4f);
        list.add(4.6f);
        list.add(3.1f);
        list.add(8.91f);
        list.add(5.43f);
        list.add(2.7f);
        list.add(9.0f);
        list.selectionSort();
        String a = String.valueOf(list);
        assertEquals("(7)root={data=2.7, sig->{data=3.1, sig->{data=4.6, sig->{data=5.4, sig->{data=5.43, sig->{data=8.91, sig->{data=9.0, sig->null}}}}}}}",a);
    }
}
