package Tarea14;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestPush {
    @Test
    public void testPushString(){
        Stack<String> list = new Stack<>();
        list.push("a");
        list.push("b");
        list.push("c");
        list.push("d");
        list.push("e");
        String letters = String.valueOf(list.push("f"));
        assertEquals("{data=a, sig->{data=b, sig->{data=c, sig->{data=d, sig->{data=e, sig->{data=f, sig->null}}}}}}",letters);
    }
    @Test
    public void testPushInteger(){
        Stack<Integer> list = new Stack<>();
        list.push(1);
        list.push(2);
        list.push(3);
        list.push(4);
        list.push(5);
        String letters = String.valueOf(list.push(6));
        assertEquals("{data=1, sig->{data=2, sig->{data=3, sig->{data=4, sig->{data=5, sig->{data=6, sig->null}}}}}}",letters);
    }
}