package Tarea14;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestPop {
    @Test
    public void testPopString(){
        Stack<String> list = new Stack<>();
        list.push("a");
        list.push("b");
        list.push("c");
        list.push("d");
        list.push("e");
        String letters = String.valueOf(list.push("f"));
        // a,b,c,d,e,f
        assertEquals("{data=a, sig->{data=b, sig->{data=c, sig->{data=d, sig->{data=e, sig->{data=f, sig->null}}}}}}",letters);
        letters = String.valueOf(list.pop());
        // a,b,c,d,e
        assertEquals("{data=a, sig->{data=b, sig->{data=c, sig->{data=d, sig->{data=e, sig->null}}}}}",letters);
        letters = String.valueOf(list.pop());
        // a,b,c,d
        assertEquals("{data=a, sig->{data=b, sig->{data=c, sig->{data=d, sig->null}}}}",letters);
        letters = String.valueOf(list.pop());
        // a,b,c
        assertEquals("{data=a, sig->{data=b, sig->{data=c, sig->null}}}",letters);
        letters = String.valueOf(list.pop());
        // a,b
        assertEquals("{data=a, sig->{data=b, sig->null}}",letters);
        letters = String.valueOf(list.pop());
        // a
        assertEquals("{data=a, sig->null}",letters);
        letters = String.valueOf(list.pop());
        // null
        assertEquals("null",letters);
    }
    @Test
    public void testPopInteger(){
        Stack<Integer> list = new Stack<>();
        list.push(1);
        list.push(2);
        list.push(3);
        list.push(4);
        list.push(5);
        String letters = String.valueOf(list.push(6));
        // 1,2,3,4,5,6
        assertEquals("{data=1, sig->{data=2, sig->{data=3, sig->{data=4, sig->{data=5, sig->{data=6, sig->null}}}}}}",letters);
        letters = String.valueOf(list.pop());
        // 1,2,3,4,5
        assertEquals("{data=1, sig->{data=2, sig->{data=3, sig->{data=4, sig->{data=5, sig->null}}}}}",letters);
        letters = String.valueOf(list.pop());
        // 1,2,3,4
        assertEquals("{data=1, sig->{data=2, sig->{data=3, sig->{data=4, sig->null}}}}",letters);
        letters = String.valueOf(list.pop());
        // 1,2,3
        assertEquals("{data=1, sig->{data=2, sig->{data=3, sig->null}}}",letters);
        letters = String.valueOf(list.pop());
        // 1,2
        assertEquals("{data=1, sig->{data=2, sig->null}}",letters);
        letters = String.valueOf(list.pop());
        // 1
        assertEquals("{data=1, sig->null}",letters);
        letters = String.valueOf(list.pop());
        // null
        assertEquals("null",letters);
    }
}