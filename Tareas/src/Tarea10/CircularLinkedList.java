package Tarea10;

class Node<T> {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data;}
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() { return "data=" + data; }
}
public class CircularLinkedList<T> implements List<T> {
    private Node<T> head;
    private Node<T> last = head;
    private int size;
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean add(T data) {
        Node newNode = new Node(data);
        if (head == null){
            head = newNode;
        }else {
            last.setNext(newNode);
        }
        last = newNode;
        last.setNext(head);
        size++;
        return true;
    }

    @Override
    public boolean remove(T data) {
        Node<T> node = head;
        Node<T> node1 = head;
        while (node.getData() != data) {
            if (node.getNext() == head)
                return false;
            node1 = node;
            node = node.getNext();
        }
        if (node == head) {
            while (node1.getNext() != head)
                node1 = node1.getNext();
            head = node.getNext();
            node1.setNext(head);
            size--;
            return true;
        } else if (node.getNext() == head) {
            node1.setNext(head);
            size--;
            return true;
        } else {
            node1.setNext(node.getNext());
            size--;
            return true;
        }
    }

    @Override
    public T get(int index) {
        if (isEmpty() || index < 0)
            return null;
        else {
            Node<T> node = head;
            for (int i = 0; i < index ; i++, node = node.getNext());

            return node.getData();
        }
    }

    public static List<Integer> dummyList(int s) {
        CircularLinkedList<Integer> l = new CircularLinkedList<>();
        l.size = s;
        l.head = new Node<>(l.size);
        dummyNode(l.head, --s, null);
        return l;
    }
    private static Node<Integer> dummyNode(Node<Integer> n, int d, Node<Integer> z) {
        z = z != null ? z : n;
        if (d <= 0) { n.setNext(z); return n;}
        else n.setNext(dummyNode(new Node<>(d), d -1, z));
        return  n;
    }
}
