package Tarea7;

import java.util.*;

public class JUArrayList implements List<Integer> {
    private int [] integers = new int[4];
    private int count = 0;
    @Override
    public int size() {
        for (int i : integers) {
            count++;
        }
        return count;
    }

    @Override
    public boolean isEmpty() {
        integers[0] = 1;
        for (int i : integers) {
            if (integers[i] != 0){
                count++;
            }
            if (count == 0){
                return true;
            }
            else return false;
        }
        return false;
    }
    @Override
    public Iterator<Integer> iterator() {
        integers[0]=2;
        integers[1]=3;
        int a = 0;
        for (int i : integers) {
            Iterator<Integer> iterator1 = new Iterator<Integer>() {
                @Override
                public boolean hasNext() {
                    return count > 0;
                }

                @Override
                public Integer next() {
                    return integers[a + 1];
                }
            };
        }
        return iterator();
    }
    @Override
    public boolean add(Integer integer) {
        if (count == integers.length){
            int[] datos2=new int[integers.length+1];
            for (int i = 0; i < integers.length; i++) {
                datos2[i] = integers[i];
            }
            integers = datos2;
        }
        integers[count] = integer;
        count++;
        return false;
    }

    @Override
    public boolean remove(Object o) {
        boolean verifyElement = false;
        int object1 = (int) o;
        int count = 0;
        for (int i = 0; i < integers.length; i++) {
            if (integers[i] == object1 && count < 1) {
                verifyElement = true;
                count++;
            }
            if (verifyElement && i < integers.length - 1) {
                integers[i] = integers[i+1];
            }
        }

        return verifyElement;
    }
    @Override
    public void clear() {
        Arrays.fill(integers, 0);
    }

    @Override
    public Integer get(int index) {
        int [] integers = {0,1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
        int number = integers[index];
        return number;
    }
    @Override
    public Integer remove(int index) {
        Integer [] integers = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
        Integer integer = integers[index];
        integers[index]=null;
        for (int i = 0; i < integers.length; i++) {
            if (integers[i] == null){
                integers[i] = integers[i+1];
            }
        }
        return integer;
    }
    //------------------------------------------

    @Override
    public boolean contains(Object o) {
        return false;
    }
    @Override
    public Integer set(int index, Integer element) {
        return null;
    }

    @Override
    public void add(int index, Integer element) {

    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }
    //-----------------------------------------------

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        return null;
    }
}
