package Tarea14;

public class Stack<T extends Comparable<T>>{
    private Node<T> head;
    private Node<T> aux;
    private int size = 0;
    private Node<T> first;
    public Node<T> push(T data){
        Node<T> node = new Node<>(data,null);
        if (size == 0) {
            first = node;
        } else {
            aux = first;
            while (aux.getNext() != null) {
                aux = aux.getNext();
            }
            aux.setNext(node);
        }
        size++;
        head = first;
        return head;
    }
    public Node<T> pop() {
        if (size == 1){
            aux = first;
            first = null;
            size--;
            return aux;
        }
        aux = first;
        Node<T> ultimo = aux;
        while (aux.getNext() != null) {
            ultimo = aux;
            aux = aux.getNext();
        }
        ultimo.setNext(null);
        size--;
        return aux;
    }
    public boolean isEmpty(){
        return head == null;
    }
    public int size(){
        return size;
    }
    @Override
    public String toString() {
        return "Stack"+ head;
    }
}