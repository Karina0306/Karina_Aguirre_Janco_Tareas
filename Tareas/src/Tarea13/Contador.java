package Tarea13;

public class Contador {
    public static void contarHasta(int contador) { // for i = contador
        System.out.println(contador);
        if (contador > 0) { //while i > 0
            //contarHasta(contador - 1); // i--
            contarHasta(--contador); // i--
        }
    }
}

