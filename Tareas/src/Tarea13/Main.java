package Tarea13;
import Tarea13.Contador;
import Tarea13.Infinito;
import Tarea13.LlamadasEncadenadas;
import Tarea13.RecursividadvsIteracion;
public class Main {
    public static void main(String[] args) {
        // 1.- sacar screenshot de la salida del metodo
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour
        LlamadasEncadenadas.doOne();

        // descomentar la llamda a: Infinito.whileTrue()
        // 1.- sacar screenshot de la exception
        // 2.- a continuacion describir el problema:
        /* Descripcion: El promela que encontramos es que al llamar al métod whileTrue este siempre va a correr
        y no hay nada que lo haga detenerse ni para que el ciclo se detenga por lo que seria un ciclo infinito
         dende se llama a si mismo uno y otra vez sin fin y no deja que corran lo demás.*/
        // 3.- volver a comentar la llmanda a: Infinito.whileTrue()
        // Infinito.whileTrue();

        // descomentar la llamda a: Contador.contarHasta(), pasando un humer entero positivo
        // 1.- sacar screenshot de la salida del metodo
        // 2.- a continuacion describir como funciona este metodo:
        /* Descripcion:El método pide que se ingrese un numero entero positivo y se lo asigna a la variable contador
        se imprime el cotador con el numero ingresado como primer dato y entra en un if donde su condición es que
        sea mayor a 0 ,si es el caso se llama al mismo metodo pero el valor seria el contador descontado en uno.
        luego se imprime de nuevo el contador y asi hasta que no se compla la condición del if.*/
        Contador.contarHasta(3);
        System.out.println(RecursividadvsIteracion.factorialIter(5));
        System.out.println(RecursividadvsIteracion.factorialRecu(5));
    }
}