package Tarea15;

public class AccountNumberNotFoundException extends Exception {
    public AccountNumberNotFoundException  (String errorMessage) {
        super(errorMessage);
    }
}