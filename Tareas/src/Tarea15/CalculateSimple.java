package Tarea15;
import Tarea14.Node;
import Tarea14.Stack;
public class CalculateSimple {
    // Estoy oomitiendo la restricción del uso del String, ya que uso el String.ValueOf para
    // poder concatenar los digitos y formar números enteros.
    // No pude  realizar de los rquisitos las prioridades de las operaciones.
    public static Stack<Character> getTokens(String str) {
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            tokens.push(str.charAt(i));
        }
        return tokens;
    }
    public int sumaSimple(String a){
        int result = 0;
        Stack<Character> digits = getTokens(a);
        Character num1 = digits.pop().getData();
        Character character = '+';
        while (digits.size()>0){
            Node node1 = digits.pop();
            if (node1.getData().equals(character)){
                Character num2 = digits.pop().getData();
                result += Integer.parseInt(String.valueOf(num1))+Integer.parseInt(String.valueOf(num2));
            }
            num1= '0';
        }return result;}
    public int multiplySimple(String a) {
        int result = 1;
        Stack<Character> simbolos = getTokens(a);
        Character num1 = simbolos.pop().getData();
        Character d = '*';
        while (simbolos.size()>0){
            Node node1 = simbolos.pop();
            if (node1.getData().equals(d)){
                Character num2 = simbolos.pop().getData();
                result *= Integer.parseInt(String.valueOf(num1))*Integer.parseInt(String.valueOf(num2));
            }
            Character aux = '1';
            num1 = aux;
        }
        return result;
    }
    public int enhanceSimple(String a){
        int result = 1;
        Stack<Character> elementos = getTokens(a);
        Character num1 = elementos.pop().getData();
        Character d = '^';
        while (elementos.size()>0){
            Node node1 = elementos.pop();
            if (node1.getData().equals(d)){
                Character num2 = elementos.pop().getData();
                for (int i = 0; i < Integer.parseInt(String.valueOf(num1)); i++) {
                    result *= Integer.parseInt(String.valueOf(num2));
                }
            }
        }
        return result;
    }
    public int factorialSimple(String a){
        int result = 1;
        Stack<Character> elementos = getTokens(a);
        Character d = '!';
        while (elementos.size()>0){
            Node node1 = elementos.pop();
            if (node1.getData().equals(d)){
                Character number = elementos.pop().getData();
                for (int i = Integer.parseInt(String.valueOf(number)); i > 0; i--) {
                    result *= Integer.parseInt(String.valueOf(number));
                    number--;
                }
            }
        }
        return result;
    }
}