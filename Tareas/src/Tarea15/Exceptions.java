package Tarea15;

public class Exceptions {
    public boolean verifyRange(int number) throws AccountNumberNotFoundException {
        int numbersRange = 2147483647;
        if (number >= 0 && number <= numbersRange)
            return true;
        else {
            throw new AccountNumberNotFoundException  ("Not a positive integer: " + number );
        }
    }
    public boolean verifyTypeVariable(String numbers) throws AccountNumberNotFoundException {
        for (int i = 0; i < numbers.length(); i++) {
            if (numbers.charAt(i) == '.' || numbers.charAt(i) == ',')
                throw new AccountNumberNotFoundException  ("Not a positive integer: " + numbers);
        }
        return true;
    }
    public boolean verifyBrackets(String exercise) throws AccountNumberNotFoundException {
        int countFirstBracket = 0;
        int countLastBracket = 0;
        char element, elementBefore, elementNext;
        for (int i = 0; i < exercise.length(); i++) {
            element = exercise.charAt(i);
            if (i == exercise.length()) {elementNext = exercise.charAt(i + 1);}
            else elementNext = ' ';
            if (element == '(' && elementNext != ')'){ return true;}
            if (i > 0) elementBefore = exercise.charAt(i - 1);
            else elementBefore = ' ';
            if (element == ')' && elementBefore != '(') throw new AccountNumberNotFoundException("It has more brackets!!");
            if (element == '(') countFirstBracket++;
            if (element == ')') countLastBracket++;
        }
        if (exercise.charAt(0) == ')') return true;
        if (countFirstBracket == countLastBracket) throw new AccountNumberNotFoundException("It has more brackets!!");
        else return true;
    }
    public boolean verifyOperationPermitted(String operation)throws AccountNumberNotFoundException{
        for (int i = 0; i < operation.length(); i++) {
            if (operation.charAt(i) == '/' || operation.charAt(i) == '-') {
                i = operation.length();
                throw new AccountNumberNotFoundException("operations not allowed");
            }
        }
        return true;
    }

    public boolean verifyNotLetters(String operation)throws AccountNumberNotFoundException{
        for (int i = 0; i < operation.length(); i++) {
            if ((operation.charAt(i) >= 'a' && operation.charAt(i) <= 'z') || (operation.charAt(i) >= 'A' && operation.charAt(i) <= 'Z')) {
                i = operation.length();
                throw new AccountNumberNotFoundException("Not is a number , is a letter.");
            }
        }
        return true;
    }
}
