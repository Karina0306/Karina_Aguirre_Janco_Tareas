package Tarea15;
import Tarea14.Node;
import Tarea14.Stack;

public class CalculateWithTwoDigits {
    public static Stack<Character> getTokens(String str) {
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            tokens.push(str.charAt(i));
        }
        return tokens;
    }
    private static Stack<Character> reverse(Stack<Character> model) {
        Stack<Character> result = new Stack<>();
        while (model.size() > 0) {
            result.push(model.pop().getData());
        }
        return result;
    }
    public int addTwoDigits(String a){
        int result = 0;
        Stack<Character> digits = getTokens(a);
        Stack<Character> digits2 = reverse(getTokens(a));
        Character num1 = digits.pop().getData();
        Character character = '+';
        String numAux = String.valueOf(num1);
        while (digits.size()>0){
            Node node1 = digits.pop();
            String segundoDigito = String.valueOf(num1);
            while (node1.getData() != character) {
                numAux = node1.getData() + String.valueOf(segundoDigito);
                node1 = digits.pop();
                segundoDigito = numAux;
            }if (digits.size() < 2) {
                if (node1.getData().equals(character)) {
                    Character num2 = digits.pop().getData();
                    result += Integer.parseInt(numAux) + Integer.parseInt(String.valueOf(num2));
                }num1 = '0';
            }if (digits.size() > 1){
                result = concatenateReverseDigitsSuma(digits2,numAux,'+');break;}
        }return result;
    }
    private int concatenateReverseDigitsSuma(Stack digits2, String secondNumber, Character character){
        int result = 0;
        Character num3 = (Character) digits2.pop().getData();
        while (digits2.size()>0) {
            Node node2 = digits2.pop();
            String m1 = String.valueOf(num3);
            String numAux1 = String.valueOf(num3);
            while (node2.getData() != character) {
                numAux1 = String.valueOf(m1) + node2.getData();
                node2 = digits2.pop();
                m1 = numAux1;
            }
            result += Integer.parseInt(secondNumber) + Integer.parseInt(numAux1);
            break;
        }
        return result;
    }
    public int multiplyTwoDigits(String a){
        int result = 1;
        Stack<Character> digits = getTokens(a);
        Stack<Character> digits2 = reverse(getTokens(a));
        Character num1 = digits.pop().getData();
        Character character = '*';
        String numAux = String.valueOf(num1);
        while (digits.size()>0){
            Node node1 = digits.pop();
            String segundoDigito = String.valueOf(num1);
            while (node1.getData() != character) {
                numAux = node1.getData() + String.valueOf(segundoDigito);
                node1 = digits.pop();
                segundoDigito = numAux;
            }if (digits.size() < 2) {
                if (node1.getData().equals(character)) {
                    Character num2 = digits.pop().getData();
                    result *= Integer.parseInt(numAux) * Integer.parseInt(String.valueOf(num2));
                }num1 = '1';
            }if (digits.size() > 1){
                result = concatenateReverseDigitsMultiply(digits2,numAux,'*');break;}
        }return result;
    }
    private int concatenateReverseDigitsMultiply(Stack digits2, String secondNumber, Character character){
        int result = 1;
        Character num3 = (Character) digits2.pop().getData();
        while (digits2.size()>0) {
            Node node2 = digits2.pop();
            String m1 = String.valueOf(num3);
            String numAux1 = String.valueOf(num3);
            while (node2.getData() != character) {
                numAux1 = String.valueOf(m1) + node2.getData();
                node2 = digits2.pop();
                m1 = numAux1;
            }
            result *= Integer.parseInt(secondNumber) * Integer.parseInt(numAux1);
            break;
        }
        return result;
    }
    public int enhanceTwoDigits(String a){
        int result = 1;
        Stack<Character> elementos = getTokens(a);
        Stack<Character> elementos2 = reverse(getTokens(a));
        String num = concatenateReverseDigits(elementos2,'^');
        Character num1 = elementos.pop().getData();
        Character character = '^';
        String numAux="";
        while (elementos.size()>0){
            Node node1 = elementos.pop();
            String segundoDigito = "";
            while (node1.getData() != character) {
                numAux = node1.getData() + segundoDigito;
                node1 = elementos.pop();
                segundoDigito = numAux;break;
            }
            if (node1.getData().equals(character)) {
                for (int i = 0; i < Integer.parseInt(String.valueOf(num1)); i++) {
                    result *= Integer.parseInt(String.valueOf(num));
                }
            }
        }return result;
    }
    private String concatenateReverseDigits(Stack digits2, Character character){
        Character num3 = (Character) digits2.pop().getData();
        String numAux1 = String.valueOf(num3);
        while (digits2.size()>0) {
            Node node2 = digits2.pop();
            String m1 = String.valueOf(num3);
            numAux1 = String.valueOf(num3);
            while (node2.getData() != character) {
                numAux1 = String.valueOf(m1) + node2.getData();
                node2 = digits2.pop();
                m1 = numAux1;
            }break;
        }
        return numAux1;
    }
    public int factorialTwoDigits(String a){
        int result = 1;
        Stack<Character> elementos = getTokens(a);
        Stack<Character> elementos1 = reverse(getTokens(a));
        Character d = '!';
        while (elementos.size()>0){
            Node node1 = elementos.pop();
            if (node1.getData().equals(d)){
                String num = concatenateReverseDigits(elementos1,'!');
                int number = Integer.parseInt(num);
                for (int i = Integer.parseInt(String.valueOf(number)); i > 0; i--) {
                    result *= Integer.parseInt(String.valueOf(number));
                    number--;
                }
            }
        }
        return result;
    }
}
