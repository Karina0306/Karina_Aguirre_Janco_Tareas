package Tarea15;
import Tarea14.Node;
import Tarea14.Stack;

public class MixedOperations extends CalculateSimple{
    public static Stack<Character> getTokens(String str) {
        Stack<Character> tokens = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            tokens.push(str.charAt(i));
        }
        return tokens;
    }
    private static Stack<Character> reverse(Stack<Character> model) {
        Stack<Character> result = new Stack<>();
        while (model.size() > 0) {
            result.push(model.pop().getData());
        }
        return result;
    }
    public int OperationsSumaYMultiply(String a){
        int result = 0;
        Stack<Character> digits = reverse(getTokens(a));
        Character character = '+';
        Character character1 = '*';
        Character num2 = digits.pop().getData();
        while (digits.size()>0){
            Node node = digits.pop();
            if (node.getData().equals(character)){
                int num = Integer.parseInt(String.valueOf(digits.pop().getData()));
                int num1 = Integer.parseInt(String.valueOf(num2));
                result += num+num1 ;
                num2 = '0';
            }
            if (node.getData().equals(character1)){
                int num = Integer.parseInt(String.valueOf(digits.pop().getData()));
                result *= num;
            }
        }return  result;
    }
    public int OperationsSumaYPotencia(String a){
        int result = 0;
        Stack<Character> digits = reverse(getTokens(a));
        Character character = '+';
        Character character1 = '^';
        Character num2 = digits.pop().getData();
        while (digits.size()>0){
            Node node = digits.pop();
            if (node.getData().equals(character)){
                int num = Integer.parseInt(String.valueOf(digits.pop().getData()));
                int num1 = Integer.parseInt(String.valueOf(num2));
                result += num+num1 ;
                num2 = '0';
            }
            if (node.getData().equals(character1)){
                int num = Integer.parseInt(String.valueOf(digits.pop().getData()));
                for (int i = num-1; i > 0; i--) {
                    result *= result;
                }
            }
        }return  result;
    }
    public int OperationsSumaYFactorial(String a){
        int result = 0;
        Stack<Character> digits = reverse(getTokens(a));
        Character character = '+';
        Character character1 = '!';
        Character num2 = digits.pop().getData();
        while (digits.size()>0){
            Node node = digits.pop();
            if (node.getData().equals(character)){
                int num = Integer.parseInt(String.valueOf(digits.pop().getData()));
                int num1 = Integer.parseInt(String.valueOf(num2));
                result += num+num1 ;
                num2 = '0';
            }
            if (node.getData().equals(character1)){
                for (int i = result-1; i > 0; i--) {
                    result *= i;
                }
            }
        }return  result;
    }
}