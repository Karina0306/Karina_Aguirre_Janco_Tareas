package Tarea11;

public class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node<T> root;
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;

        return true;
    }
    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }
    public void selectionSort() {
        Node<T> node = root;
        for (int i = 0; i < size - 1; i++) {
            int indexMin = i;
            T indexData = node.getData();
            Node<T> nextNode = node.getNext();
            for (int j = i + 1; j < size; j++) {
                T nextData =  nextNode.getData();
                if (indexData.compareTo(nextData) > 0) {
                    indexMin = j;
                    indexData = nextData;
                }
                nextNode = nextNode.getNext();
            }
            xchange(indexMin, i);
            node = getNode(i).getNext();
        }
    }

    public void bubbleSort() {
        boolean sorted;
        do {
            sorted = true;
            for (int i=0;i<size-1;i++){
                if (getNode(i).getData().compareTo(getNode(i+1).getData())>0){
                    xchange(i,i+1);
                    sorted = false;
                }
            }
        }while (!sorted);
    }

    @Override
    public void xchange(int y, int x) {
        T auxX;
        T auxY;
        Node<T> node = root;
        for (int i = 0; i < x; i++){
            node = node.getNext();
        }
        auxX = node.getData();
        node = root;
        for (int i = 0; i < y; i++){
            node = node.getNext();
        }
        auxY = node.getData();
        node.setData(auxX);
        node = root;
        for (int i= 0; i<x; i++){
            node = node.getNext();
        }
        node.setData(auxY);
    }
    private Node<T> getNode(int index) {
        Node<T> node = root;
        for (int i = 0; i < size; i++) {
            if (i == index) {
                return node;
            }
            node = node.getNext();
        }
        return node;
    }
}