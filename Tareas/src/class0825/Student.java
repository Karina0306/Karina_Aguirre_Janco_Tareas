package class0825;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
public class Student implements Comparable<Student> {
    String name;
    int score;
    public Student(String name, int score) {
        this.name = name;
        this.score = score;
    }
    // by score
    // by name
    // by score and name
    public int compareTo(Student o) {
        //return score -o.score;
        //return Integer.compare(score,o.score);
        //return name.compareTo(o.name);
        name.compareTo(o.name);
        if (name == o.name){
            return score - o.score;
        }
        return name.toLowerCase().compareTo(o.name.toLowerCase());
    }
    public String toString() {
        return "name:" + name + ", score: " + score;
    }
}
