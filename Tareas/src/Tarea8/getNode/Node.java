package Tarea8.getNode;

public class Node {
    public static int data;
    public static Node next = null;

    public static int getNth(Node n, int index) throws Exception{
        int count = 0;
        if(n == null || index < 0)
            throw new IllegalArgumentException();

        while (n != null && index > 0){
            n = n.next;
            index--;
        }
        if(n == null)
            throw new IllegalArgumentException();

        for (int i = 0; i < index; i++) {
            count++;
            if (count == index){
                return n.data;
            }
        }
        return n.data;
    }
}
