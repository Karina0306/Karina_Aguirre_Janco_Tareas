package Tarea8.appendNode;

public class Node2 {

    int data;
    Node2 next = null;

    Node2(final int data) {
        this.data = data;
    }

    public Node2(int data, Node2 next) {
        this.data = data;
        this.next = next;
    }

    public static Node2 append(Node2 listA, Node2 listB) {
        if ( listA == null ) {
            return listB;
        }
        Node2 tmp = listA;
        while ( tmp.next != null ) {
            tmp = tmp.next;
        }
        tmp.next = listB;
        return listA;
    }
}
