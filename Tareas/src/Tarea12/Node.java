package Tarea12;

public class Node<T> {
    private T data;
    private Node<T> next;
    private Node<T> prev;
    public Node(T data) { this.data = data;}
    public Node(T data, Node<T> prev , Node<T> next){
        this.data = data;
        this.next = next;
        this.prev = prev;
    }
    public T getData() { return data;}
    public Node<T> getNext() { return next; }

    public Node<T> getPrev() {
        return prev;
    }

    public void setPrev(Node<T> prev) {
        this.prev = prev;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}

