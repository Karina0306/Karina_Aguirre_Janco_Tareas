package Tarea12;

public class DoublyCircularLinkedList <T extends Comparable<T> > implements List<T> {
    private Node<T> head;
    private int size;
    @Override
    public int size() {
        int count = 0;
        if (head == null)
            return count;
        else {
            Node<T> temp = head;
            do {
                temp = temp.getNext();
                count++;
            } while (temp != head);
        }
        return count;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean add(T data) {
        if (data == null){
            return false;
        }
        if (head == null){
            head = new Node<>(data);
            head.setNext(head);
        } else {
            Node<T> node = head;
            int index = 1;
            while (index < size){
                node = node.getNext();
                index++;
            }
            node.setNext(new Node<>(data, node, head));
            head.setPrev(node.getNext());
        }
        size++;
        return true;
    }
    // Method for adding data to the top of the list
    // For unit tests to add to the beginning of the list
    public void addAtTheBeginning(T data){
        Node<T> newNode = new Node<>(data);
        newNode.setPrev(head.getPrev());
        newNode.setNext(head);
        head.getPrev().setNext(newNode);
        head.setPrev(newNode);
        head = newNode;
        size++;
    }

    @Override
    public boolean remove(T data) {
        if (head != null) {
            Node<T> node = head;
            int index = 0;
            while (node.getData() != data) {
                node = node.getNext();
                index++;
                if (index >= size) return false;
            }
            if (head.getData() == data) {
                head = head.getNext();
                size--;
                node.getPrev().setNext(head);
                return true;
            }
            node.getPrev().setNext(node.getNext());
            size--;
            return true;
        }
        return false;
    }

    @Override
    public T get(int index) {
        if (isEmpty()) return null;
        if (isEmpty() || index < 0)
            return null;
        else {
            Node<T> node = head;
            for (int i = 0; i < index ; i++, node = node.getNext());

            return node.getData();
        }
    }
//------------------ New Method ----------------------------------------
    public void xchange(int y, int x) {
        if (x == y || x < 0 || y < 0) { return; }
        T xData = getNode(x).getData();
        T yData = getNode(y).getData();
        setNode(x, yData);
        setNode(y, xData);
    }
    private Node<T> setNodeData(int pos, Node node, T data, int index) {
        if (pos == index) {
            Node<T> newNode = new Node(data);
            newNode.setNext(node.getNext());
            return newNode;
        }
        index++;
        Node nextNode = setNodeData(pos, node.getNext(), data, index);
        node.setNext(nextNode);
        return node;
    }
    private void setNode(int pos, T data) {
        head = setNodeData(pos, head, data, 0);
    }
    private Node<T> getNode(int index) {
        Node<T> node = head;
        for (int i = 0; i < size; i++) {
            if (i == index) {
                return node;
            }
            node = node.getNext();
        }
        return node;
    }
//----------------------------------------------------------------------
    @Override
    public void selectionSort() {}
    @Override
    public void bubbleSort() {}
    @Override
    public void mergeSort() {}
}