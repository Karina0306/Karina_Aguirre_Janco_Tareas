package class0209;

import class0825.Student;

public class Node <T>{
    private T data;
    private Node<T> next;

    public void setData(T data) {this.data = data;}
    public Node(T data, Node<T> next) { this.data = data; this.next = next; }
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}
