package class0209;

public class Queue<T extends Comparable<T>> {
    Node<T> head;
    Node<T> tail = head;
    public void add (T data){
        Node<T> newNode = new Node<T>(data,tail);
        if(head == null){
            head = newNode;
        }else{
            newNode.setNext(tail);
        }
        tail = newNode;
    }
    public Node<T> remove(){
        Node<T> node = tail;
        Node<T> aux = head ;
        if (node == head){
            node = null;
            head = node;
            tail = head;
            return aux;
        }
        for (node = tail ; node!= null;node = node.getNext() ) {
            if (node.getNext().equals(head) ){
                node.setNext(null);
                head = node;
            }
        }
        return aux;
    }

    @Override
    public String toString() {
        return "Queue{" + tail +
                '}';
    }

    public boolean verify(){
        for ( Node<T> node = tail ; node != null; node = node.getNext()) {
            Node<T> node1 = tail;
            if (node1 == null){
                break;
            }
            for (node1 = tail.getNext(); node1 != null; node1 = node1.getNext()) {
                if (node.getData().compareTo(node1.getData())==0) {
                    return false;
                }
            }
            tail = tail.getNext();
        }
        return true;
    }
}
