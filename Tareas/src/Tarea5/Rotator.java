package Tarea5;

public class Rotator {
    private int n;
    private Object newIndex;
    public Object[] rotate(Object[] data, int n) {
        this.n = n;
        this.newIndex = newIndex;
        if(n > 0) {
            for(int i = 0; i < n; i++) {
                newIndex = data[data.length - 1];
                for(int j = data.length - 2; j >= 0; j--) {
                    data[j + 1] = data[j];
                }
                data[0] = newIndex;
            }
        } else {
            for (int i = 0; i < n * -1; i++) {
                newIndex = data[0];
                for (int j = 1; j < data.length; j++) {
                    data[j - 1] = data[j];
                }
                data[data.length - 1] = newIndex;
            }
        }
        return data;
    }
}
