package class0830;

public class queue {
    NodeInt head;
    NodeInt tail = head;
    public void add (int data){
        NodeInt newNode = new NodeInt(data,tail);
        if(head == null){
            head = newNode;
        }else{
            newNode.next = tail;
        }
        tail = newNode;
    }
    public NodeInt remove(){
        NodeInt node = tail;
        NodeInt aux = head ;
        if (node == head){
            node = null;
            head = node;
            tail = head;
            return aux;
        }
        for (node = tail ; node!= null;node = node.next ) {
            if (node.next.equals(head) ){
                node.next = null;
                head = node;
            }
        }
        return aux;
    }

    @Override
    public String toString() {
        return "queue{" +
                "tail=" + tail +
                '}';
    }

    public static void main(String[] args) {
        queue cola = new queue();
        cola.add(1);
        cola.add(2);
        cola.add(3);
        cola.add(4);
        System.out.println(cola);
        System.out.println(cola.remove());
        System.out.println(cola);
        System.out.println(cola.remove());
        System.out.println(cola);
        System.out.println(cola.remove());
        System.out.println(cola);
        System.out.println(cola.remove());
        System.out.println(cola);
        System.out.println(cola.head);
    }
}
