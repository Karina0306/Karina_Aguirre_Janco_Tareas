package class0830;

public class NodeInt {
    public int data;
    public NodeInt next;
    public NodeInt(int data, NodeInt next) { this.data = data; this.next = next; }
    public String toString() {
        return "[data=" + data + ", next->" + next + "]";
    }
}
