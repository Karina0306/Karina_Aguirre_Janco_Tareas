package Tarea9;

public class Nodo<T> {
    T dato;
    Nodo<T> sig;
    public Nodo(T dato) {
        this.dato = dato;
    }

    public String toString() {
        return "{dato=" + dato + ", sig->" + sig + "}";
    }
}
