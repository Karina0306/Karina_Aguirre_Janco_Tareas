package Tarea9;
import java.util.*;

public class JULinkedList<T> implements List<T> {
    private Nodo<T> raiz;
    private Nodo<T> ultimo;
    private int size;
    @Override
    public int size() {
        Nodo temp = raiz;
        int count = 0;
        while (temp != null) {
            count++;
            temp = temp.sig;
        }
        return count;
    }

    @Override
    public boolean isEmpty() {
        if (raiz == null)
            return true;
        //if(size()==0){return true;}
        return false;
    }

    @Override
    public boolean contains(Object o) {
        if (raiz.dato.equals(o)) return true;
        for (Nodo<T> anterior = raiz; anterior.sig != null; anterior = anterior.sig) {
            if (anterior.sig.dato.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Nodo<T> current = raiz;
            @Override
            public boolean hasNext() {
                return current != null;
            }
            @Override
            public T next() {
                if(hasNext()){
                    T data = current.dato;
                    current = current.sig;
                    return data;
                }
                return null;
            }
        };
    }

    @Override
    public boolean add(T t) {
        Nodo<T> nodo = new Nodo<>(t);
        if (t == null){
            return false;
        }
        if (ultimo == null){
            raiz = nodo;
        }else {
            ultimo.sig = nodo;
        }
        ultimo = nodo;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (isEmpty()) return false;
        if (raiz.dato.equals(o)) {
            raiz = raiz.sig;
            size--;
            return true;
        }
        for (Nodo<T> anterior = raiz; anterior.sig != null; anterior = anterior.sig) {
            if (anterior.sig.dato.equals(o)) {
                anterior.sig = anterior.sig.sig;
                size--;
                return true;
            }
        }
        return false;
    }

    @Override
    public void clear() {
        raiz = null;
    }

    @Override
    public T get(int i) {
        if (i >= 0 && i < size){
            Nodo<T> node = raiz;
            for (int j = 0; j < size && j != i; j++) {
                node = node.sig;
            }
            return node.dato;
        }
        return null;
    }

    @Override
    public T set(int i, T t) {
        if (i >= 0 && i < size){
            Nodo<T> node = raiz;
            for (int j = 0; j < size && j != i; j++) {
                node = node.sig;
                node.dato = t;
            }
            return node.dato;
        }
        return null;
    }

    @Override
    public T remove(int i) {
        Nodo<T> n1;
        if (i ==0) {
            n1 = raiz;
            raiz = raiz.sig;
        }else {
            Nodo<T> n = raiz;
            for (int j = 0 ; j<i-1;j++ ){
                n=n.sig;
            }
            n1 = n.sig;
            n.sig = n1.sig;
        }
        return n1.dato;
    }

    @Override
    public int indexOf(Object o) {
        int index = -1;
        int index2 = 0;
        Nodo<T> current =raiz;
        while (current!=null){
            if (current.dato.equals(o)){
                index = index2;
                break;
            }
            current = current.sig;
            index2++;
        }
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index = -1;
        int size = size();
        int count = 0;

        Nodo<T> current = raiz;

        for (int i = 0; i < size; i++) {
            if (o == current.dato) {index = count;}
            count++;
            current = current.sig;
        }
        return index;
    }
    //-----------------------------------------
    @Override
    public Object[] toArray() {
        Object[] array = new Object[size()];
        int i = 0;
        for (T t : this){
            array[i] = t;
            i++;
        }
        return array;
    }
    @Override
    public <T1> T1[] toArray(T1[] t1s) {
        return null;
    }

    @Override
    public List<T> subList(int i, int i1) {
        return null;
    }
    @Override
    public void add(int i, T t) {

    }
//-------------------------------------
    @Override
    public boolean containsAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        return false;
    }

    @Override
    public boolean addAll(int i, Collection<? extends T> collection) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }
    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int i) {
        return null;
    }
}
