package Tarea6;

public class Movements {
    private int x;
    private int y;
    Player player = new Player();

    public String[][] movRight(String [][] arrangement,int x, int y){
        arrangement[x][y] = arrangement[x][y+1] ="O ";
        arrangement[x][y] = "- ";
        return arrangement;
    }
    public String[][] movLeft(String [][] arrangement,int x, int y){
        arrangement[x][y] = arrangement[x][y-1] ="O ";
        arrangement[x][y] = "- ";
        return arrangement;
    }
    public String[][] movUp(String [][] arrangement, int x, int y){
        arrangement[x][y] = arrangement[x-1][y] ="O ";
        arrangement[x][y] = "- ";
        return arrangement;
    }
    public String[][] movDown(String [][] arrangement,int x, int y){
        arrangement[x][y] = arrangement[x+1][y] ="O ";
        arrangement[x][y] = "- ";
        return arrangement;
    }
}
