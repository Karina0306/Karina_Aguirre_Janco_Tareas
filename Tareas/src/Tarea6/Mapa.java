package Tarea6;

public class Mapa {
    private String [][] arr;
    private int x;
    private int y;

    public String[][] mapGenerate(String [][] arr){
        this.arr = arr;
        Player player = new Player();
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = "- ";
                arr[0][3] = "X ";
                arr[2][4] = "X ";
                arr[4][7] = "X ";
                arr[7][8] = "X ";
                arr[8][6] = "X ";
                player.positionPlayer(arr,3,2);
            }
        }
        return arr;
    }
    public String[][] printMap(String [][] arr){
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
        return arr;
    }
}
